//
// SnapshotDemo is a brief example to show how to interpret data 
// read back from the Snapshot command.
// 
// To further process numerics such as voltage/iout/temperature
// into a readable floating value, use the pmbus numeric conversions 
// library at: https://bitbucket.org/ericssonpowermodules/c-pmbus-numeric-conversions
//

// Copyright © Ericsson AB 2014

// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a
// compiled binary, for any purpose, commercial or non-commercial, and
// by any means.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// #include <argp.h>

#include "../vendor/snapshot-parameter-capture/src/SnapshotData.h"
#include "../vendor/snapshot-parameter-capture/src/StatusBitmasks.h"

#include "../vendor/c-pmbus-numeric-conversions/src/PMBusNumericConversions.h"

int asciiHexToHex(char *inputString, unsigned int inputLength, unsigned char **outBuffer, unsigned int *outLength );

void BMR464XXX8_printSnapshotData(POL2008E_SnapshotData *snapshotData);

void BMR464XXX8_printStatusMFR(uint8_t statusMFR);
void BMR464XXX8_printStatusVOut(uint8_t statusVOut);
void BMR464XXX8_printStatusIOut(uint8_t statusIOut);
void BMR464XXX8_printStatusInput(uint8_t statusInput);
void BMR464XXX8_printStatusTemperature(uint8_t statusTemperature);
void BMR464XXX8_printStatusCML(uint8_t statusCML);

int main(int argc, char **argv)
{

  char str[65];
  int error;
  unsigned char *hexBuffer;
  unsigned int hexLength;
  int i;

  BMR464XXX8_SnapshotData snapshotData;

  // for (;;)
  // {
    
    hexBuffer = NULL;
    error = scanf("%64s", str);

    printf("Snapshot parse utility - alpha - intended for BMR464 XXX8\n");
    printf("============================\n");
    printf("Input string: %s\n", str);

    if(strlen(str) != 64)
    {
      printf("Invalid length to parse snapshot\n");
      return 1;
    }
  
    error = asciiHexToHex(&str[0], strlen(str), &hexBuffer, &hexLength );
      
    if(error != 0)
      printf("Hex conversion error\n");
    else
    {
      BMR464XXX8_SnapshotBytesToData(&hexBuffer[0], &snapshotData);
      BMR464XXX8_printSnapshotData(&snapshotData);
    }

    
    if(hexBuffer != NULL)
      free (hexBuffer);
  // }

    return 0;
}


int asciiHexToHex(char *inputString, unsigned int inputLength, unsigned char **outBuffer, unsigned int *outLength )
{
  int i;
  int error;
  unsigned char temp;
    
  if(inputLength % 2)
  {
    printf("invalid length to convert to hex\n");
    return -1;
  }

  *outLength = inputLength / 2;
  *outBuffer = malloc(*outLength);

  for(i = 0; i < *outLength; i++)
  {
    
    error = sscanf(inputString + (2*i), "%02x", (unsigned int *)(*outBuffer + i));  
    if(error != 1)
    {
      return -1;
    }
  }

  return 0;
}


void BMR464XXX8_printSnapshotData(POL2008E_SnapshotData *snapshotData)
{

    const signed char voutExponent = 0x13;

    printf("============================\n");
    printf("Snapshot Data \n");
    printf("============================\n");

    printf("Input Voltage: %f\n",
          PMBusLinearToFloat((unsigned char) (snapshotData->InputVoltage >> 8) ,
                             (unsigned char) (snapshotData->InputVoltage) ) 
          );

    printf("Output Voltage: %f\n",
      PMBusUnsignedVOutLinearToFloat_shortMantissa( voutExponent, snapshotData->OutputVoltage)
      );


  printf("LoadCurrent: %f\n",
        PMBusLinearToFloat((unsigned char) (snapshotData->LoadCurrent >> 8) ,
                           (unsigned char) (snapshotData->LoadCurrent) ) 
          );

  printf("PeakCurrent: %f\n", 
        PMBusLinearToFloat((unsigned char) (snapshotData->PeakCurrent >> 8) ,
                           (unsigned char) (snapshotData->PeakCurrent) ) 
          );
  printf("DutyCycle: %f\n", 
        PMBusLinearToFloat((unsigned char) (snapshotData->DutyCycle >> 8) ,
                           (unsigned char) (snapshotData->DutyCycle) ) 
          );
  printf("InternalTemperature: %f\n", 
        PMBusLinearToFloat((unsigned char) (snapshotData->InternalTemperature >> 8) ,
                           (unsigned char) (snapshotData->InternalTemperature) ) 
          );

  printf("SwitchingFrequency: %f\n", 
        PMBusLinearToFloat((unsigned char) (snapshotData->SwitchingFrequency >> 8) ,
                           (unsigned char) (snapshotData->SwitchingFrequency) ) 
          );

  printf("StatusVOut: \n");
  BMR464XXX8_printStatusVOut(snapshotData->StatusVOut);

  printf("StatusIOut: \n");
  BMR464XXX8_printStatusIOut(snapshotData->StatusIOut);

  printf("StatusInput: \n");
  BMR464XXX8_printStatusInput(snapshotData->StatusInput);

  printf("StatusTemperature: \n");
  BMR464XXX8_printStatusTemperature(snapshotData->StatusTemperature);

  printf("StatusCML: \n");
  BMR464XXX8_printStatusCML(snapshotData->StatusCML);

  printf("StatusMFR: \n");
  BMR464XXX8_printStatusMFR(snapshotData->StatusMFR);
  
}


// void BMR464SnapshotSample()
// {
//   //sample bytes assuming they are read directly back from the smbus transaction
//   //such that byte 0 is snapshotSampleReadback[0]
//   uint8_t snapshotSampleReadback[32] = { 0x20, 0xD3, //bytes 1:0 - input voltage - 12.5 
//                 0x9A, 0x69, //bytes 3:2 - output voltage - 3.3 (assume -13 vout_mode linear exp)
//                 0x00, 0xBB, //bytes 5:4 - load current - 
//                 0x00, 0xC2, //bytes 7:6 - peak current 
//                 0x00, 0x00, //bytes 9:8 - duty cycle
//                 0x20, 0xDB, //bytes 11:10 - internal temperature
//                 0x00, 0x00, //bytes 13:12 - reserved 
//                 0x80, 0xFA, //bytes 15:14 - switching frequency
//                 0x00,        //byte 16 - status_vout 
//                 0x00,        //byte 17 - status_iout 
//                 0xC0,        //byte 18 - status_input 
//                 0x00,        //byte 19 - status_temperature 
//                 0x00,        //byte 20 - status_cml 
//                 0x00,        //byte 21 - status_mfr
//                 0x00, 0x00, //bytes 31:22 - reserved 
//                 0x00, 0x00, 
//                 0x00, 0x00,
//                 0x00, 0x00, 
//                 0x00, 0x00};

//   BMR464XXX8_SnapshotData snapshotData;

//   BMR464XXX8_SnapshotBytesToData(&snapshotSampleReadback[0], &snapshotData);

//   printf( "Input voltage (hex): %02X \n", snapshotData.InputVoltage);
//   BMR464XXX8_processStatusInput(snapshotData.StatusInput);
// }


// void BMR465SnapshotSample()
// {
//   //sample bytes assuming they are read directly back from the smbus transaction
//   //such that byte 0 is snapshotSampleReadback[0]
//   uint8_t snapshotSampleReadback[32] = { 0x20, 0xD3, //bytes 1:0 - input voltage - 12.5 
//                 0x9A, 0x69, //bytes 3:2 - output voltage - 3.3 (assume -13 vout_mode linear exp)
//                 0x00, 0xBB, //bytes 5:4 - load current - 
//                 0x00, 0xC2, //bytes 7:6 - peak current 
//                 0x00, 0x00, //bytes 9:8 - duty cycle
//                 0x20, 0xDB, //bytes 11:10 - internal temperature
//                 0x00, 0x00, //bytes 13:12 - reserved 
//                 0x80, 0xFA, //bytes 15:14 - switching frequency
//                 0x00,        //byte 16 - status_vout 
//                 0x00,        //byte 17 - status_iout 
//                 0xC0,        //byte 18 - status_input 
//                 0x00,        //byte 19 - status_temperature 
//                 0x00,        //byte 20 - status_cml 
//                 0x00,        //byte 21 - status_mfr
//                 0x00,        //byte 22 - status_nvm 
//                 0x00, 0x00, //bytes 24:23 - reserved
//                 0x00, 0x00, //bytes 26:25 Load current phase 0 
//                 0x00, 0x00, //bytes 28:27 Load current phase 1
//                 0x00, 0x00, 0x00}; // bytes 31:29 - reserved

//   BMR465XX10_SnapshotData snapshotData;

//   BMR465XX10_SnapshotBytesToData(&snapshotSampleReadback[0], &snapshotData);

//   printf( "Input voltage (hex): %02X \n", snapshotData.InputVoltage);
//   BMR465XX10_processStatusInput(snapshotData.StatusInput);
// }

void BMR464XXX8_printStatusMFR(uint8_t statusMFR)
{
  if(statusMFR & BMR464XXX8_StatusMFR_bVMONOVFault)
    printf("VMON OV Fault\n");

  if(statusMFR & BMR464XXX8_StatusMFR_bVMONUVFault)
    printf("VMON UV Fault\n");

  if(statusMFR & BMR464XXX8_StatusMFR_bReserved2)
    printf("Reserved2 \n");

  if(statusMFR & BMR464XXX8_StatusMFR_bLossOfExternalSyncFault)
    printf("Loss Of External Sync Fault\n");

  if(statusMFR & BMR464XXX8_StatusMFR_bVMONOVWarning)
    printf("VMON OV Warning\n");

  if(statusMFR & BMR464XXX8_StatusMFR_bVMONUVWarning)
    printf("VMON UV Warning\n");

  if(statusMFR & BMR464XXX8_StatusMFR_bReserved6)
    printf("Reserved6 \n");

  if(statusMFR & BMR464XXX8_StatusMFR_bReserved7)
    printf("Reserved7 \n");

  if(statusMFR == 0x00)
    printf("No MFR faults detected\n");

}

void BMR464XXX8_printStatusVOut(uint8_t statusVOut)
{
  if(statusVOut & BMR464XXX8_StatusVOut_bTOffMaxWarning)
    printf("TOff Max Warning\n");

  if(statusVOut & BMR464XXX8_StatusVOut_bTOnMaxFault)
    printf("TOn Max Fault\n");

  if(statusVOut & BMR464XXX8_StatusVOut_bVOutMaxWarning)
    printf("VOut Max Warning\n");

  if(statusVOut & BMR464XXX8_StatusVOut_bVOutUVFault)
    printf("VOut UV Fault\n");

  if(statusVOut & BMR464XXX8_StatusVOut_bVOutOVFault)
    printf("VOut OV Fault\n");

  if(statusVOut == 0x00)
    printf("No VOut faults detected\n");
}

void BMR464XXX8_printStatusIOut(uint8_t statusIOut)
{
  if(statusIOut & BMR464XXX8_StatusIOut_bIOutUCFault)
    printf("IOut UC Fault\n");

  if(statusIOut & BMR464XXX8_StatusIOut_bIOutOCFault)
    printf("IOut OC Fault\n");

  if(statusIOut == 0x00)
    printf("No IOut faults detected\n");
}

void BMR464XXX8_printStatusInput(uint8_t statusInput)
{

  if(statusInput & BMR464XXX8_StatusInput_bVInUVFault)
    printf("VIn UV Fault \n");

  if(statusInput & BMR464XXX8_StatusInput_bVInUVWarning)
    printf("VIn UV Warning \n");

  if(statusInput & BMR464XXX8_StatusInput_bVInOVWarning)
    printf("VIn OV Warning \n");

  if(statusInput & BMR464XXX8_StatusInput_bVInOVFault)
    printf("VIn OV Fault \n");

  if(statusInput == 0x00)
    printf("No input faults detected\n");
}

void BMR464XXX8_printStatusTemperature(uint8_t statusTemperature)
{
  if(statusTemperature & BMR464XXX8_StatusTemperature_bUTFault)
    printf("UT Fault\n");

  if(statusTemperature & BMR464XXX8_StatusTemperature_bUTWarning)
    printf("UT Warning\n");

  if(statusTemperature & BMR464XXX8_StatusTemperature_bOTWarning)
    printf("OT Warning\n");

  if(statusTemperature & BMR464XXX8_StatusTemperature_bOTFault)
    printf("OT Fault\n");

  if(statusTemperature == 0x00)
    printf("No temperature faults detected\n");
}

void BMR464XXX8_printStatusCML(uint8_t statusCML)
{
  if(statusCML & BMR464XXX8_StatusCML_bOtherMemoryOrLogicFault)
    printf("Other Memory Or Logic Fault\n");

  if(statusCML & BMR464XXX8_StatusCML_bOtherCommunicationFault)
    printf("Other Communication Fault\n");

  if(statusCML & BMR464XXX8_StatusCML_bProcessorFaultDetected)
    printf("Processor Fault Detected\n");

  if(statusCML & BMR464XXX8_StatusCML_bMemoryFaultDetected)
    printf("Memory Fault Detected\n");

  if(statusCML & BMR464XXX8_StatusCML_bPacketErrorCheckFailed)
    printf("Packet Error Check Failed\n");

  if(statusCML & BMR464XXX8_StatusCML_bInvalidOrUnsupportedData)
    printf("Invalid Or Unsupported Data\n");

  if(statusCML & BMR464XXX8_StatusCML_bInvalidOrUnsupportedCommand)
    printf("Invalid Or Unsupported Command\n");

  if(statusCML == 0x00)
    printf("No CML faults detected\n");
}


// void BMR465XX10_processStatusInput(uint8_t statusInput)
// {

//   if(statusInput & BMR465XX10_StatusInput_bVInUVFault)
//     printf("VIn UV Fault \n");

//   if(statusInput & BMR465XX10_StatusInput_bVInUVWarning)
//     printf("VIn UV Warning \n");

//   if(statusInput & BMR465XX10_StatusInput_bVInOVWarning)
//     printf("VIn OV Warning \n");

//   if(statusInput & BMR465XX10_StatusInput_bVInOVFault)
//     printf("VIn OV Fault \n");

// }



