#readme.md

This is a snapshot utility. It's intended to be used with the smbus tool for reading out snapshot data. See gif below for a visual explanation:

![](./snapshotutil.gif)


##building the util for windows (on cygwin / mingw32) 

Compiler should be set to use mingw32 to work on the windows commandline (gcc no longer supports -mno-cygwin). In a cygwin session, do the following:

cd snapshot-parse-utility
export CC=x86_64-w64-mingw32-gcc.exe
make
