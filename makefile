SOURCES=$(wildcard src/**/*.c src/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

SNAPSHOT_PARSE_UTIL=$(patsubst %.c,%,$(SOURCES))
# SNAPSHOT_PARSE_UTIL_EXEC=main

SNAPSHOT_LIB_SOURCES=$(wildcard vendor/snapshot-parameter-capture/src/**/*.c vendor/snapshot-parameter-capture/src/*.c)
SNAPSHOT_LIB_OBJECTS=$(patsubst %.c,%.o,$(SNAPSHOT_LIB_SOURCES))

PMBUS_NUMERIC_CONVERSIONS_LIB=$(wildcard vendor/c-pmbus-numeric-conversions/build/*.a)  
#PMBUS_NUMERIC_CONVERSIONS_SOURCES=$(wildcard vendor/c-pmbus-numeric-conversions/src/**/*.c vendor/c-pmbus-numeric-conversions/src/*.c)
#PMBUS_NUMERIC_CONVERSIONS_OBJECTS=$(patsubst %.c,%.o,$(PMBUS_NUMERIC_CONVERSIONS_SOURCES))

CFLAGS+=-std=C99

# add link to glibc library search paths (needed for powf calls to work on Linux)
LOPTFLAGS += -lm

all: snapshotParseUtil


.PHONY: snapshotParseUtil
snapshotParseUtil: snapshot_lib pmbus_numeric_conversions
	$(CC) -g $(SOURCES) $(SNAPSHOT_LIB_OBJECTS) $(PMBUS_NUMERIC_CONVERSIONS_LIB) -o snapshot_parse_util $(LOPTFLAGS)

snapshot_lib: 
	cd ./vendor/snapshot-parameter-capture/ && $(MAKE)

pmbus_numeric_conversions: 
	cd ./vendor/c-pmbus-numeric-conversions/ && $(MAKE)

clean:
	rm -rf build $(OBJECTS) $(SNAPSHOT_LIB_OBJECTS)
	find . -name "*.gc*" -exec rm {} \;
	rm -rf `find . -name "*.dSYM" -print`
	rm -f src/main
	cd ./vendor/c-pmbus-numeric-conversions/ && $(MAKE) clean
	cd ./vendor/snapshot-parameter-capture/ && $(MAKE) clean



